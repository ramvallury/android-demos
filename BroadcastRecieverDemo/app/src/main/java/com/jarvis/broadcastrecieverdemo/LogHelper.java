package com.jarvis.broadcastrecieverdemo;

import android.util.Log;

/**
 * Created by vvallury on 11/7/2014.
 */
public class LogHelper {
    public static void customLog(String label)
    {
        Log.i("com.jarvis", label);
    }
}
