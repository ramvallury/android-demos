package com.jarvis.broadcastrecieverdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by vvallury on 11/7/2014.
 */
public class ServiceBroadCastReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if(action.equals(Intent.ACTION_POWER_CONNECTED)) {
            Toast.makeText(context.getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
            // write code here to enabled airplane mode
        }
        else if(action.equals(Intent.ACTION_POWER_DISCONNECTED))
        {
            Toast.makeText(context.getApplicationContext(), "Disconnected", Toast.LENGTH_LONG).show();
            // write code here to disable airplane mode
        }
    }
}
